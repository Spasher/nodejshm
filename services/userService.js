const { UserRepository } = require('../repositories/userRepository');

class UserService {
    createUser(data,res){
        const item = UserRepository.create(data)
        return res.status(200).json({message:"User created",user:item})
    }
    getUsers(data,res,next){
        const item = UserRepository.getAll();
        return res.status(200).json({Users:item})
    }
    search(data,res) {
        const id = data.params.id
        const item = UserRepository.getOne(id);
        if(!item) {
            return res.status(404).json({message:"Not found user"});
        }
        return res.status(200).json({User:item})
    }
    updateUser(data,res){
        const id = data.params.id
        const item = UserRepository.update(id,data)
        if(!item) {
            return res.status(404).json({message:"Error update"});
        }
        return res.status(200).json({User:item})

    }
    deleteUser(data,res){
        const id = data.params.id
        const item = UserRepository.delete(id)
        if(!item) {
            return res.status(404).json({message:"Error delete"});
        }
        return res.status(200).json({User:item})

    }
}

module.exports = new UserService();