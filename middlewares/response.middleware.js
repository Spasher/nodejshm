const responseMiddleware = (req, res, next) => {
    return res.status(200).json(req.data)
}

exports.responseMiddleware = responseMiddleware;