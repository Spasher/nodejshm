const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    // TODO: Implement methods to work with fighters
    createFighter(data,res){
        if (!data.body.hasOwnProperty('health')){
            data.body.health = 100
        }
        const item = FighterRepository.create(data)
        return res.status(200).json({message:"Fighter created",fighter:item})
    }
    getFighters(data,res,next){
        const item = FighterRepository.getAll();
        return res.status(200).json({Fighters:item})
    }
    search(data,res) {
        const id = data.params.id
        const item = FighterRepository.getOne(id);
        if(!item) {
            return res.status(404).json({message:"Fighter Not found "});
        }
        return res.status(200).json({User:item})
    }
    updateFighter(data,res){
        const id = data.params.id
        const item = FighterRepository.update(id,data)
        if(!item) {
            return res.status(404).json({message:"Error update"});
        }
        return res.status(200).json({User:item})

    }
    deleteFighter(data,res){
        const id = data.params.id
        const item = FighterRepository.delete(id)
        if(!item) {
            return res.status(404).json({message:"Error delete"});
        }
        return res.status(200).json({User:item})

    }

}

module.exports = new FighterService();