const { fighter } = require('../models/fighter');
const { FighterRepository } = require('../repositories/fighterRepository');
const createFighterValid = (req, res, next) => {
    const {power,defense ,health,name } = req.body
    const fighters = FighterRepository.getAll();
    let chek = true
    try{
        if(name === undefined || name === null){
            res.status(400).json({error: true, message: "Uncorrect name"})
            return;
        }
        Object.keys(req.body).forEach((key) => {
            if(!fighter.hasOwnProperty(key)) {
                res.status(400).json({error: true, message: "Uncorrect arrtibutes"})
                chek = false
            }
        })

        fighters.forEach((fighter)=> {
            if (fighter["name"] === name) {
                res.status(400).json({error: true, message: "Name is used"})
                chek = false
            }
        })
        if(health !== undefined) {
            if (!(health < 120 && health > 80) || typeof(health) !== "number") {
                res.status(400).json({error: true, message: "Uncorrect health "})
                return
            }
        }

        if(!(power < 100 && power > 1) || typeof(power) !== "number"){
            res.status(400).json({error: true,message: "Uncorrect power "})
            return
        }
        if(!(defense < 10 && defense > 1) || typeof(defense) !== "number"){
            res.status(400).json({error: true,message: "Uncorrect defense "})
            return

        }


    }catch (e) {

    }
    if(chek){
        next();
    }


}

const updateFighterValid = (req, res, next) => {
    const {power, defense, health, name} = req.body
    const fighters = FighterRepository.getAll();
    let chek = true
    try {
        Object.keys(req.body).forEach((key) => {
            if (!fighter.hasOwnProperty(key)) {
                res.status(400).json({error: true, message: "Uncorrect arrtibutes"})
                chek = false
            }
        })

        fighters.forEach((fighter) => {
            if (fighter["id"] !== req.params.id) {
                if (fighter["name"] === name) {
                    res.status(400).json({error: true, message: "Name is used"})
                    chek = false
                }
            }
        })
        if (health !== undefined) {
            if (!(health < 120 && health > 80) || typeof (health) !== "number") {
                res.status(400).json({error: true, message: "Uncorrect health "})
                return
            }
        }

        if (!(power < 100 && power > 1) || typeof (power) !== "number") {
            res.status(400).json({error: true, message: "Uncorrect power "})
            return
        }
        if (!(defense < 10 && defense > 1) || typeof (defense) !== "number") {
            res.status(400).json({error: true, message: "Uncorrect defense "})
            return

        }

    } catch (e) {

    }

    if (chek) {
        next();
    }
}
exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;